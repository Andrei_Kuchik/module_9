﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqToDB;
using MicroORM;
using MicroORM.Entities;

namespace ConsoleApplication
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			using (var db = new DBNorthwind("Northwind"))
			{
				//GetListOfProductsWithProperties(db);
				//GetListOfEmployeesWithRegion(db);
				//GetStatisticsOfRegions(db);
				//GetListEmployeesWithSupplier(db);
				////---------------------------------------
				AddNewEmployeeWithListOfTerritories(db);
				//UpdateProductCategory(db, 1, 2);
				//InsertProductsWithCategoryAndSupplier(db);
				//ChangeProductInOrder(db);
			}

			Console.ReadKey();
		}

		private static void GetListOfProductsWithProperties(DBNorthwind db)
		{
			foreach (var product in db.Products
				.LoadWith(t => t.Category)
				.LoadWith(t => t.Supplier))
			{
				Console.WriteLine(product.ProductName);
				Console.WriteLine(product.Category?.CategoryName);
				Console.WriteLine(product.Supplier?.CompanyName);
				Console.WriteLine("---------------------------------------------------");
			}
		}

		private static void GetListOfEmployeesWithRegion(DBNorthwind db)
		{
			var employeesWithRegions = db.Employees
				.Join(db.EmployeeTerritories
						.Join(
					db.Territories
						.LoadWith(x => x.Region),
					leftItem => leftItem.TerritoryID,
					rightItem => rightItem.TerritoryID,
					(leftItem, rightItem) => new
					{
						leftItem.EmployeeID,
						rightItem.Region.RegionDescription
					}),
				leftItem => leftItem.EmployeeID,
				rightItem => rightItem.EmployeeID,
				(leftItem, rightItem) => new
				{
					EmployeeID = leftItem.LastName,
					Region = rightItem.RegionDescription
				}).Distinct()
				.GroupBy(x => x.EmployeeID);

			foreach (var employee in employeesWithRegions)
			{
				Console.WriteLine($"Employee lastname - {employee.Key}");
				foreach (var region in employee)
				{
					Console.WriteLine($"Region - {region.Region}");
				}

				Console.WriteLine("-----------------------------------------");
			}
		}

		private static void GetStatisticsOfRegions(DBNorthwind db)
		{
			var regions = db.EmployeeTerritories
				.Join(db.Territories
						.LoadWith(x => x.Region),
					leftItem => leftItem.TerritoryID,
					rightItem => rightItem.TerritoryID,
					(leftItem, rightItem) => new
					{
						Employee = leftItem.EmployeeID,
						Region = rightItem.Region.RegionDescription
					})
				.Distinct()
				.GroupBy(key => key.Region,
				(myKey, collection) => new
				{
					Region = myKey,
					Count = collection.Count()
				});
			foreach (var region in regions)
			{
				Console.WriteLine($"Region - {region.Region}");
				Console.WriteLine($"Count - {region.Count}");
			}
		}

		private static void GetListEmployeesWithSupplier(DBNorthwind db)
		{
			var employeesWithListSuppliers = db.Orders
				.LoadWith(x => x.Employee)
				.LoadWith(x => x.Shipper)
				.Select(x => new
				{
					Employee = x.Employee.LastName,
					Shipper = x.Shipper.CompanyName
				})
				.Distinct()
				.GroupBy(x => x.Employee);

			foreach (var employee in employeesWithListSuppliers)
			{
				Console.WriteLine($" Employee - {employee.Key}");
				foreach (var supplier in employee)
				{
					Console.WriteLine(supplier.Shipper);
				}
				Console.WriteLine("-----------------------------------------");
			}
		}

		private static void AddNewEmployeeWithListOfTerritories(DBNorthwind db)
		{
			db.Insert(new EmployeeTerritory
			{
				EmployeeID = Convert.ToInt32(db.InsertWithIdentity(
					new Employee
				{
					Region = "Western",
					FirstName = "Andrei",
					LastName = "Kuchik",
					BirthDate = DateTime.Now,
					HireDate = DateTime.Now
					})),
				TerritoryID = "03049"
			});
		}

		private static void UpdateProductCategory(DBNorthwind db, int oldValue, int newValue)
		{
			db.Products.Where(x => x.CategoryID == oldValue)
				.Set(y => y.CategoryID, newValue)
				.Update();
		}

		private static void InsertProductsWithCategoryAndSupplier(DBNorthwind db)
		{
			var listProduct = new List<Product>
			{
				new Product
				{
					ProductName = "Prod 1",
					Category = new Category
					{
						CategoryName = "Categ number 1"
					},
					Supplier = new Supplier
					{
						CompanyName = "Sup number 1"
					}
				},

				new Product
				{
					ProductName = "Prod 2",
					Category = new Category
					{
						CategoryName = "Categ number 2"
					},
					Supplier = new Supplier
					{
						CompanyName = "Sup number 2"
					}
				}
			};

			foreach (var product in listProduct)
			{
				if (db.Categories
					    .FirstOrDefault(y => y.CategoryName == product.Category.CategoryName) == null)
				{
					db.InsertWithIdentity(
						new Category
						{
							CategoryName = product.Category.CategoryName
						});
				}

				if (db.Suppliers
					    .FirstOrDefault(x => x.CompanyName == product.Supplier.CompanyName) == null)
				{
					db.InsertWithIdentity(
						new Supplier
						{
							CompanyName = product.Supplier.CompanyName
						});
				}

				product.CategoryID = db.Categories
					.FirstOrDefault(x => x.CategoryName == product.Category.CategoryName)
					.CategoryID;
				product.SupplierID = db.Suppliers
					.FirstOrDefault(x => x.CompanyName == product.Supplier.CompanyName)
					.SupplierID;

				db.InsertWithIdentity(new Product
				{
					ProductName = product.ProductName,
					CategoryID = product.CategoryID,
					SupplierID = product.SupplierID
				});
			}
		}

		private static void ChangeProductInOrder(DBNorthwind db)
		{
			foreach (var order in db.Orders
				.Where(x => x.ShippedDate == null))
			{
				foreach (var ordInfo in db.OrderDetails
					.Where(x => x.OrderID == order.OrderID))
				{
					var categoryId = (int)db.Products
						.FirstOrDefault(x => x.ProductID == ordInfo.ProductID)
						.CategoryID;
					var prod = db.Products
						.FirstOrDefault(x =>
						x.CategoryID == categoryId && x.ProductID != ordInfo.ProductID);
					if (prod != null)
					{
						ordInfo.ProductID = prod.ProductID;
					}
				}
			}
		}
	}
}