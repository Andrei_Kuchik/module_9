﻿using System.Data.Entity.Migrations;

namespace EF.Migrations
{
	public class Northwind13Schema : DbMigration
	{
		public override void Up()
		{
			DropForeignKey("Northwind.Territories", "RegionID", "Northwind.Region");
			DropIndex("Northwind.Territories", new[] {"RegionID"});
			CreateTable(
					"Northwind.Regions",
					c => new
					{
						RegionsID = c.Int(false),
						RegionDescription = c.String(false, 50)
					})
				.PrimaryKey(t => t.RegionsID);

			AddColumn("Northwind.Customers", "FoundationDate", c => c.DateTime(false));
			AddColumn("Northwind.Territories", "Region_RegionsID", c => c.Int());
			CreateIndex("Northwind.Territories", "Region_RegionsID");
			AddForeignKey("Northwind.Territories", "Region_RegionsID", "Northwind.Regions", "RegionsID");
			DropTable("Northwind.Region");
		}

		public override void Down()
		{
			CreateTable(
					"Northwind.Region",
					c => new
					{
						RegionID = c.Int(false),
						RegionDescription = c.String(false, 50)
					})
				.PrimaryKey(t => t.RegionID);

			DropForeignKey("Northwind.Territories", "Region_RegionsID", "Northwind.Regions");
			DropIndex("Northwind.Territories", new[] {"Region_RegionsID"});
			DropColumn("Northwind.Territories", "Region_RegionsID");
			DropColumn("Northwind.Customers", "FoundationDate");
			DropTable("Northwind.Regions");
			CreateIndex("Northwind.Territories", "RegionID");
			AddForeignKey("Northwind.Territories", "RegionID", "Northwind.Region", "RegionID", true);
		}
	}
}