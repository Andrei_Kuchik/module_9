﻿using System.Data.Entity.Migrations;

namespace EF.Migrations
{
	public class Northwind11Schema : DbMigration
	{
		public override void Up()
		{
			CreateTable(
					"Northwind.CreditCards",
					c => new
					{
						CreditCardID = c.Int(false, true),
						CardNumber = c.Int(false),
						ExperationDate = c.DateTime(false),
						CardHolder = c.String(false),
						Customer_CustomerID = c.String(maxLength: 5)
					})
				.PrimaryKey(t => t.CreditCardID)
				.ForeignKey("Northwind.Customers", t => t.Customer_CustomerID)
				.Index(t => t.Customer_CustomerID);
		}

		public override void Down()
		{
			DropForeignKey("Northwind.CreditCards", "Customer_CustomerID", "Northwind.Customers");
			DropIndex("Northwind.CreditCards", new[] {"Customer_CustomerID"});
			DropTable("Northwind.CreditCards");
		}
	}
}