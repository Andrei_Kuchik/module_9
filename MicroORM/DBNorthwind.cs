﻿using LinqToDB;
using LinqToDB.Data;
using MicroORM.Entities;

namespace MicroORM
{
	public class DBNorthwind : DataConnection
	{
		public DBNorthwind(string configuration) : base(configuration)
		{
		}

		public ITable<Customer> Customers => GetTable<Customer>();
		public ITable<Category> Categories => GetTable<Category>();
		public ITable<CustomerCustomerDemo> CustomerCustomerDemos => GetTable<CustomerCustomerDemo>();
		public ITable<CustomerDemographic> CustomerDemographics => GetTable<CustomerDemographic>();
		public ITable<Employee> Employees => GetTable<Employee>();
		public ITable<EmployeeTerritory> EmployeeTerritories => GetTable<EmployeeTerritory>();
		public ITable<Order> Orders => GetTable<Order>();
		public ITable<OrderDetails> OrderDetails => GetTable<OrderDetails>();
		public ITable<Product> Products => GetTable<Product>();
		public ITable<Shipper> Shippers => GetTable<Shipper>();
		public ITable<Supplier> Suppliers => GetTable<Supplier>();
		public ITable<Territory> Territories => GetTable<Territory>();
	}
}